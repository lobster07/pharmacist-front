
const webpackMerge = require('webpack-merge'),
commonConfig = require('./webpack.build.js'),
config = require('./app.config.json'),
SshWebpackPlugin = require('ssh-webpack-plugin');

module.exports = webpackMerge(commonConfig, {
    plugins: [
        new SshWebpackPlugin(config.deploy)
    ]
});
