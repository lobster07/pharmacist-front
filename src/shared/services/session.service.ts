/**
 * Created by lobster07 on 24.02.17.
 */

import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable} from "rxjs";
import * as _ from "lodash";

@Injectable()
export class SessionService{

    private _storage: any;
    private _data: BehaviorSubject<any>;

    get data():Observable<any>{
        return this._data.asObservable()
    }

    constructor(){
        this._storage = localStorage;

        let authToken = this.getAuthToken();
        let defaultData = authToken? JSON.parse(this._storage.getItem(authToken)): null;

        this._data = new BehaviorSubject<any>(defaultData);
    }

    public getAuthToken(){
        return this._storage.getItem("auth_token");
    }

    public setItem(key: string, value: any){

        if(this.getAuthToken()){

            let currentValue = this._data.getValue();
            let newValue = _.assign({}, currentValue);

            newValue[key] = value;

            this._storage.setItem(this.getAuthToken(), JSON.stringify(newValue));
            this._data.next(newValue);

        }else{
            throw Error("You can't set item if you aren't authorized");
        }
    }

    public getItem(key: string){
        if(this.getAuthToken()) return this._data.getValue()[key];
        else return null;
    }

    public setItems(data?: object){

        if(this.getAuthToken()){

            let currentValue = this._data.getValue();
            let newValue = _.assign({}, currentValue, data);

            this._storage.setItem(this.getAuthToken(), JSON.stringify(newValue));
            this._data.next(newValue);

        }else{
            throw Error("You can't set item if you aren't authorized");
        }
    }

    public removeItem(key: string){

        if(this.getAuthToken()){

            let currentValue = this._data.getValue();
            let newValue = _.omit(currentValue, [key]);

            this._storage.setItem(this.getAuthToken(), newValue);
            this._data.next(newValue);

        }else{
            throw Error("You can't remove item if you aren't authorized");
        }
    }

    public initSession(authToken: string, sessionData?: object){

        if(this.getAuthToken()){
            this.clearSession();
        }

        this._storage.setItem("auth_token", authToken);
        this.setItems(sessionData);
    }

    public clearSession(){

        this._storage.removeItem(this.getAuthToken());
        this._storage.removeItem("auth_token");

        this._data.next(null);
    }

}