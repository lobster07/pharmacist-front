/**
 * Created by lobster07 on 24.02.17.
 */

import {Injector} from "@angular/core";
import {Http, Headers, URLSearchParams, Response} from "@angular/http";
import {Observable} from "rxjs";
import * as _ from 'lodash';
import {Router} from "@angular/router";
import {SessionService} from "../session.service";

export class BaseApiClass{

    protected http: Http;
    protected router: Router;
    protected session: SessionService;

    private headers: Headers = new Headers({'Content-Type': 'application/json'});

    /**
     * Constructor
     */
    constructor(private injector: Injector){
        this.http = injector.get(Http);
        this.router = injector.get(Router);
        this.session = injector.get(SessionService);
    }

    /**
     * Form full url from part
     */
    public formUrl(url: string): string{
        return `${process.env.API}/api${url}`
    }

    /**
     * Form query for angular http service
     * Also adds auth token
     */
    private formQuery(query?: Object): URLSearchParams {

        let result = new URLSearchParams();

        if (query) {
            _.each(query, (v: any, k: string) => {
                result.set(k, v);
            })
        }

        let authToken = this.session.getAuthToken();

        if (authToken){
            result.set('token', authToken)
        }

        return result;
    }

    /**
     * process api response and check for errors
     */
    private processingResponse(res: Response): any{
        let response = res.json();
        let status = res.status;

        if (response.meta.status === 'ok'){
            return response.response;
        } else {

            let errorMessage: string = _.get(response, 'meta.error[0]')? response.meta.error[0].message : response.meta.error.message;
            /* Add processing all error type here */

            if (status == 403) {

                this.session.clearSession();
                this.router.navigate(['/login']);
            }

            throw new Error(errorMessage);
        }

    }

    /**
     * Implementation of get method
     */
    public get(url: string, query?: Object): Observable<any> {
        return this.http.request(this.formUrl(url), {
            method: 'GET',
            search: this.formQuery(query),
            headers: this.headers,
            responseType: 1
        })
            .map(r => this.processingResponse(r))
            .catch(err => this.handleError(err))
    }

    /**
     * Implementation of post method
     */
    public create(url: string, body: Object, query?: Object) {
        return this.http.request(this.formUrl(url), {
            method: 'POST',
            search: this.formQuery(query),
            headers: this.headers,
            responseType: 1,
            body: body
        })
            .map(r => this.processingResponse(r))
            .catch(err => this.handleError(err))
    }

    /**
     * Implementation of put method
     */
    public update(url: string, body: Object, query?: Object) {
        return this.http.request(this.formUrl(url), {
            method: 'PUT',
            search: this.formQuery(query),
            headers: this.headers,
            responseType: 1,
            body: body
        })
            .map(r => this.processingResponse(r))
            .catch(err => this.handleError(err))
    }

    /**
     * Implementation of delete method
     */
    public remove(url: string, query?: Object) {
        return this.http.request(this.formUrl(url), {
            method: 'DELETE',
            search: this.formQuery(query),
            headers: this.headers,
            responseType: 1
        })
            .map(r => this.processingResponse(r))
            .catch(err => this.handleError(err))
    }

    /**
     * Handling error
     */
    private handleError (error: Response): Observable<any> {
        return Observable.throw(error || 'Server error');
    }
}