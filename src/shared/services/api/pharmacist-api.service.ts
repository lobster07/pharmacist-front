/**
 * Created by lobster07 on 24.02.17.
 */

import { Injectable, Injector } from "@angular/core";
import { BaseApiClass } from "./bese-api.class";

@Injectable()
export class PharmacistApiService extends BaseApiClass{

    constructor(injector: Injector) {
        super(injector)
    }

    /**
     * Physicians API
     */

    public getPhysicians(){
        return this.get("/physicians")
    }

    public getPhysician(id: string){
        return this.get(`/physicians/${id}`)
    }

    public createPhysician(body:  any){
        return this.create(`/physicians`, body)
    }

    public updatePhysician(body:  any){
        return this.update(`/physicians/${body.id}`, body)
    }

    public removePhysician(id: string){
        return this.remove(`/physicians/${id}`)
    }


    /**
     * Drugs API
     */

    public getDrugs(){
        return this.get("/drugs")
    }

    public getDrug(id: string){
        return this.get(`/drugs/${id}`)
    }

    public createDrug(body:  any){
        return this.create(`/drugs`, body)
    }

    public updateDrug(body:  any){
        return this.update(`/drugs/${body.id}`, body)
    }

    public removeDrug(id: string){
        return this.remove(`/drugs/${id}`)
    }

    /**
     * Plans API
     */

    public getPlans(){
        return this.get("/plans")
    }

    public getPlan(id: string){
        return this.get(`/plans/${id}`)
    }

    public createPlan(body:  any){
        return this.create(`/plans`, body)
    }

    public updatePlan(body:  any){
        return this.update(`/plans/${body.id}`, body)
    }

    public removePlan(id: string){
        return this.remove(`/plans/${id}`)
    }

}