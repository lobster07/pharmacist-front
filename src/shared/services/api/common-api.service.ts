/**
 * Created by lobster07 on 24.02.17.
 */

import { Injectable, Injector } from "@angular/core";
import { BaseApiClass } from "./bese-api.class";
import * as _ from "lodash";

@Injectable()
export class CommonApiService extends BaseApiClass{

    constructor(injector: Injector) {
        super(injector)
    }

    public login(email: string, password: string){
        return this.create("/auth/login", {email, password})
    }

    public register(body: any){
        return this.create("/auth/registration",  body)
    }

    public me(){
        return this.get("/me")
    }
}