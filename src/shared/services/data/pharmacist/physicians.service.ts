/**
 * Created by lobster07 on 24.02.17.
 */

import {Injectable, Injector} from "@angular/core";

import {Physician} from "../../../entities/physician";
import {BaseDataClass} from "../base.class";
import {PharmacistApiService} from "../../api/pharmacist-api.service";

@Injectable()
export class PhysiciansDataService extends BaseDataClass{


    constructor(
        private api: PharmacistApiService
    ){
        super()
    }

    getItemType(){
        return Physician
    }

    getFetchRequest(){
        return this.api.getPhysicians()
    }

    getAddRequest(body: any){
        return this.api.createPhysician(body);
    }

    getUpdateRequest(body: any){
        return this.api.updatePhysician(body)
    }

    getRemoveRequest(id: string){
        return this.api.removePhysician(id)
    }

}