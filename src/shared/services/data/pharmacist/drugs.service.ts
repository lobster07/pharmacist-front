/**
 * Created by lobster07 on 24.02.17.
 */

import {Injectable, Injector} from "@angular/core";
import {BaseDataClass} from "../base.class";
import {Drug} from "../../../entities/drug";
import {PharmacistApiService} from "../../api/pharmacist-api.service";

@Injectable()
export class DrugsDataService extends BaseDataClass{


    constructor(
        private api: PharmacistApiService
    ){
        super();
    }

    getItemType(){
        return Drug
    }

    getFetchRequest(){
        return this.api.getDrugs()
    }

    getAddRequest(body: any){
        return this.api.createDrug(body);
    }

    getUpdateRequest(body: any){
        return this.api.updateDrug(body)
    }

    getRemoveRequest(id: string){
        return this.api.removeDrug(id)
    }

}