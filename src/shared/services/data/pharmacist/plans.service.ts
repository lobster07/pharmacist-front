/**
 * Created by lobster07 on 24.02.17.
 */

import {Injectable, Injector} from "@angular/core";
import {BaseDataClass} from "../base.class";
import {Plan} from "../../../entities/plan";
import {PharmacistApiService} from "../../api/pharmacist-api.service";

@Injectable()
export class PlansDataService extends BaseDataClass{

    constructor(
        private api: PharmacistApiService
    ){
        super();
    }

    getItemType(){
        return Plan
    }

    getFetchRequest(){
        return this.api.getPlans()
    }

    getAddRequest(body: any){
        return this.api.createPlan(body);
    }

    getUpdateRequest(body: any){
        return this.api.updatePlan(body)
    }

    getRemoveRequest(id: string){
        return this.api.removePlan(id)
    }

}