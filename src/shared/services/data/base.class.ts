
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";
import * as _ from "lodash";
import {SessionService} from "../session.service";
import {Base} from "../../entities/base";
import {Injector} from "@angular/core";

export class BaseDataClass{

    private _store: Base[] = [];
    private _subject: BehaviorSubject<any[]> = new BehaviorSubject(null);
    private _fetched: boolean;

    public get store():Observable<Base[]>{
        return this._subject.asObservable().filter(res => this._fetched);
    }

    constructor(){}

    protected getItemType(): any{
        return Object;
    }

    public fetch(){

        this._fetched = false;

        this.getFetchRequest()
            .subscribe((data: any)=>{

                let newData: any[] = [];

                data.map((obj: any)=>{
                    newData.push(_.assign(new (this.getItemType())(), obj))
                });

                this._fetched = true;
                this._store = newData;

                this._subject.next(this._store);

            }, (err: Error) =>{
                this._subject.error(err);
            });
    }

    public add(item: any){

        this.getAddRequest(item).subscribe((data: any)=>{
             this.fetch();
        });
    }

    public remove(id: string){

        this.getRemoveRequest(id).subscribe(()=>{
            this.fetch();
        })
    }

    public update(obj: Base){

        let i: number = _.findIndex(this._store, {id: obj.id});

        let obs = i != -1? this.getUpdateRequest(obj):  this.getAddRequest(obj);

        obs.subscribe((res: any)=>{
            this.fetch();
        });

    }

    protected getFetchRequest():Observable<any>{
        return new Observable(null)
    }

    protected getAddRequest(data: any):Observable<any>{
        return new Observable(null)
    }
    protected getUpdateRequest(data: any):Observable<any>{
        return new Observable(null)
    }

    protected getRemoveRequest(id: string):Observable<any>{
        return new Observable(null)
    }
}


