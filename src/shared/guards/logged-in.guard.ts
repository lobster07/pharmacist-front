/**
 * Created by lobster07 on 24.02.17.
 */

import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {SessionService} from "../services/session.service";

@Injectable()
export class LoggedInGuard implements CanActivate {
    constructor(
        private router:Router,
        private session: SessionService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.checkLogin();
    }

    checkLogin(): boolean {
        if (this.session.getAuthToken()) { return true; }

        this.router.navigate(['/login']);
        return false;
    }
}
