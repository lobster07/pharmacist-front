/**
 * Created by lobster07 on 24.02.17.
 */

import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {PhysiciansDataService} from "../services/data/pharmacist/physicians.service";
import {DrugsDataService} from "../services/data/pharmacist/drugs.service";
import {PlansDataService} from "../services/data/pharmacist/plans.service";
import {SessionService} from "../services/session.service";
import {Observable} from "rxjs";

@Injectable()
export class FetchPharmacistDataGuard implements CanActivate {
    constructor(
        private router: Router,
        private session: SessionService,
        private physicians: PhysiciansDataService,
        private drugs: DrugsDataService,
        private plans: PlansDataService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

        this.physicians.fetch();
        this.drugs.fetch();
        this.plans.fetch();

        return new Observable<boolean>(observer => {
            Observable.merge(this.physicians.store, this.drugs.store, this.plans.store)
                .subscribe(()=>{
                    observer.next(true);
                }, (err: Error) => {
                    observer.next(false);
                    this.session.clearSession();
                    this.router.navigate(['/']);

                });
        });
    }
}
