import {NgModule} from "@angular/core";
import {BrowserModule}  from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {RouterModule} from "@angular/router";

import { LoggedInGuard } from "./guards/logged-in.guard";
import { FetchPharmacistDataGuard } from "./guards/fetch-data.guard";
import { CommonApiService } from "./services/api/common-api.service";
import { SessionService } from "./services/session.service";
import {DrugsDataService} from "./services/data/pharmacist/drugs.service";
import {PlansDataService} from "./services/data/pharmacist/plans.service";
import {PhysiciansDataService} from "./services/data/pharmacist/physicians.service";
import {PharmacistApiService} from "./services/api/pharmacist-api.service";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule
    ],
    providers: [
        SessionService,

        CommonApiService,
        PharmacistApiService,

        PlansDataService,
        PhysiciansDataService,
        DrugsDataService,

        LoggedInGuard,
        FetchPharmacistDataGuard
    ],
})
export class SharedModule { }

