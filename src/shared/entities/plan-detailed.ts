
import {Base} from "./base";
import {Drug} from "./drug";
import {Physician} from "./physician";


export class PhysicianItemDetailed{
    physician: Physician;
    percents: number
}

export class PlanDetailed extends Base{
    name: string = "";
    count: number = 0;
    drug: Drug;
    physiciansList: PhysicianItemDetailed[] = [];
    constructor(){
        super()
    }
}