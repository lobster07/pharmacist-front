
import {Base} from "./base";

export class PhysicianGroup extends Base{

    public name: string = "";
    public percents: number = 33;
    public persons: number=0;

    constructor(){
        super()
    }
}

export class Physician extends Base{
    name: string = "";
    description: string ="";
    persons: number = 0;
    groups: PhysicianGroup[] =[];

    constructor(){
        super()
    }
}