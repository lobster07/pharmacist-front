
import {Base} from "./base";


export class PhysicianItem{
    physicianId: string;
    percents: number
}

export class Plan extends Base{
    name: string = "";
    count: number = 0;
    drugId: string;
    physiciansList: PhysicianItem[] = [];

    constructor(){
        super()
    }
}