import { NgModule } from "@angular/core";
import { Routes, RouterModule  } from '@angular/router';
import { NotFoundComponent } from "./not-found.component";

import {LoggedInGuard} from "../shared/guards/logged-in.guard";
import {FetchPharmacistDataGuard} from "../shared/guards/fetch-data.guard";

import {PharmacistComponent} from "./pharmacist/components/pharmacist.component";
import {PhysiciansLayoutComponent} from "./pharmacist/components/sales/physicians/physicians-layout.component";
import {PhysiciansListComponent} from "./pharmacist/components/sales/physicians/physicians-list.component";
import {PhysicianAddComponent} from "./pharmacist/components/sales/physicians/physician-add.component";
import {DrugsLayoutComponent} from "./pharmacist/components/sales/drugs/drugs-layout.component";
import {DrugsListComponent} from "./pharmacist/components/sales/drugs/drugs-list.component";
import {DrugsAddComponent} from "./pharmacist/components/sales/drugs/drugs-add.component";

import {PlansLayoutComponent} from "./pharmacist/components/sales/plans/plans-layout.component";
import {PlansListComponent} from "./pharmacist/components/sales/plans/plans-list.component";
import {PlansAddComponent} from "./pharmacist/components/sales/plans/plans-add.component";
import {DashboardComponent} from "./pharmacist/components/sales/dashboard/dashboard.component";
import {LoginComponent} from "./auth/components/login.component";
import {RegistrationComponent} from "./auth/components/registration.component";
import {SalesComponent} from "./pharmacist/components/sales/sales.component";
import {VisitsComponent} from "./pharmacist/components/visits/visits.component";

let appRoutes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "pharmacist"
    },
    {
        path: "pharmacist",
        canActivate: [LoggedInGuard, FetchPharmacistDataGuard],
        component: PharmacistComponent,
        children: [
            {
                path: "",
                pathMatch: "full",
                redirectTo: "sales"
            },
            {
                path: "sales",
                component: SalesComponent,
                children: [
                    {
                        path: "",
                        pathMatch: "full",
                        redirectTo: "dashboard"
                    },
                    {
                        path: "dashboard",
                        component: DashboardComponent
                    },
                    {
                        path: "physicians",
                        component: PhysiciansLayoutComponent,
                        children: [
                            {
                                path: "",
                                component: PhysiciansListComponent
                            },
                            {
                                path: "add",
                                component: PhysicianAddComponent
                            },
                            {
                                path: ":id",
                                component: PhysicianAddComponent
                            }
                        ]
                    },
                    {
                        path: "drugs",
                        component: DrugsLayoutComponent,
                        children: [
                            {
                                path: "",
                                component: DrugsListComponent
                            },
                            {
                                path: "add",
                                component: DrugsAddComponent
                            },
                            {
                                path: ":id",
                                component: DrugsAddComponent
                            }
                        ]
                    },
                    {
                        path: "plans",
                        component: PlansLayoutComponent,
                        children: [
                            {
                                path: "",
                                component: PlansListComponent
                            },
                            {
                                path: "add",
                                component: PlansAddComponent
                            },
                            {
                                path: ":id",
                                component: PlansAddComponent
                            }
                        ]
                    }
                ]
            },
            {
                path: "visits",
                component: VisitsComponent
            }
        ]
    },

    {
        path: 'login',
        component: LoginComponent
    },

    {
        path: 'registration',
        component: RegistrationComponent
    },

    {
        path: '**',
        component: NotFoundComponent
    }

];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}