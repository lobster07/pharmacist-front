import {NgModule} from "@angular/core";
import {BrowserModule}  from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';

import { SharedModule } from "../../shared/shared.module";
import { LoginComponent} from "./components/login.component";
import { RegistrationComponent} from "./components/registration.component";


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule,
        FormsModule,
        SharedModule
    ],
    declarations: [
        LoginComponent,
        RegistrationComponent
    ]
})
export class AuthModule { }
