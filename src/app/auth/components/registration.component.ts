import { Component, OnInit, OnDestroy } from '@angular/core';
import * as _ from "lodash";
import {ActivatedRoute, Router} from "@angular/router";
import { SessionService } from "../../../shared/services/session.service";
import { CommonApiService } from "../../../shared/services/api/common-api.service";
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'registration',
    templateUrl: './registration.component.html'
})

export class RegistrationComponent implements OnInit, OnDestroy{


    public email: string ="";
    public password: string ="";
    public password_again: string="";

    public error_message: string;

    public success: boolean;

    constructor(
      private api: CommonApiService,
      private router: Router,
      private state: ActivatedRoute,
    ){}

    ngOnInit(){}

    ngOnDestroy(){}

    onSubmit(evt: any){

        this.error_message = null;

        let body = {
            email: this.email,
            password: this.password,
            password_again: this.password_again,
        };

        this.api.register(body)
            .subscribe((res: any)=>{

                this.success = true;
                Observable.timer(1500).subscribe((res)=>{
                    this.router.navigate(["../login"], {relativeTo: this.state})
                });


            },(err: Error)=>{
                this.error_message = err.message
            });

    }

    isDisabled(){
        return _.isEmpty(this.email) || _.isEmpty(this.password) || _.isEmpty(this.password_again)
    }


}