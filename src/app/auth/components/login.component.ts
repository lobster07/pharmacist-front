import { Component, OnInit, OnDestroy } from '@angular/core';
import * as _ from "lodash";
import { Router } from "@angular/router";
import { SessionService } from "../../../shared/services/session.service";
import { CommonApiService } from "../../../shared/services/api/common-api.service";

@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy{


    public email: string = "";
    public password: string = "";

    public error_message: string;

    constructor(
      private api: CommonApiService,
      private router: Router,
      private session: SessionService
    ){}

    ngOnInit(){}

    ngOnDestroy(){}

    onSubmit(evt: any){

        this.error_message = null;

        this.api.login(this.email, this.password)
            .subscribe((res: any)=>{

                this.session.initSession(res.token, {});
                this.getMe();

            }, (err: Error)=>{
                this.error_message = err.message
            });

    }

    getMe(){
        this.api.me()
            .subscribe((res: any)=>{
                this.session.setItem('me', res);
                this.router.navigate(["/"]);
            },(err)=>{
                this.error_message = err.message
            })
    }



    isDisabled(){
        return _.isEmpty(this.email) || _.isEmpty(this.password);
    }


}