import {NgModule} from "@angular/core";
import {BrowserModule}  from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from "./app.routing";

import { PharmacistModule } from "./pharmacist/pharmacist.module";
import { AuthModule } from "./auth/auth.module";

import { AppComponent } from "./app.component";
import { NotFoundComponent } from "./not-found.component";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule,

        AuthModule,
        PharmacistModule,

        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        NotFoundComponent
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
