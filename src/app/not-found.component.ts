import { Component } from '@angular/core';

@Component({
    selector: 'not-found',
    template: `<span>404, page not found</span>`
})

export class NotFoundComponent {

}