import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {Physician, PhysicianGroup} from "../../../../../shared/entities/physician";
import {PhysiciansDataService} from "../../../../../shared/services/data/pharmacist/physicians.service";

@Component({
    selector: 'physicians-list',
    templateUrl: './physicians-list.component.html'
})

export class PhysiciansListComponent implements OnInit, OnDestroy{

    private physicians: Physician[];

    private subs: Subscription;

    constructor(
        private data: PhysiciansDataService
    ){}

    ngOnInit(){
        this.subs = this.data.store
            .subscribe((arr: Physician[])=>{
            console.log(arr);
            this.physicians = arr;
        });
    }

    ngOnDestroy(){
        this.subs.unsubscribe();
    }

    shortGroup(g: PhysicianGroup): string{
        return g.name + " ("+g.percents+"%) ("+g.persons+"чл)";
    }


    onRemove(p: Physician){
        this.data.remove(p.id);
    }
}