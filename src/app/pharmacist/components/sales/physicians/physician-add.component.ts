import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import * as _ from "lodash";

import {ActivatedRoute, Params, Router} from "@angular/router";
import {Observable} from "rxjs";
import {Physician, PhysicianGroup} from "../../../../../shared/entities/physician";
import {PhysiciansDataService} from "../../../../../shared/services/data/pharmacist/physicians.service";

@Component({
    selector: 'physician-add',
    templateUrl: './physician-add.component.html'
})

export class PhysicianAddComponent implements OnInit, OnDestroy{

    private subs: Subscription;

    private physician: Physician;
    private groups: PhysicianGroup[];

    private ready: boolean;

    constructor(
        private state: ActivatedRoute,
        private router: Router,
        private data: PhysiciansDataService
    ){}

    ngOnInit(){

        this.subs = Observable.zip(
                this.state.params,
                this.data.store
            ).flatMap((res: any)=>{
                let params: Params = res[0];
                let physicians: Physician[] = res[1];
                let physician: Physician;

                if(params['id']){
                    physician = _.clone(_.find(physicians, {id: params['id']}));
                }else{
                    physician = _.assign(new Physician(),{
                        name: "Новая специализация",
                        persons: 30
                    })
                }

                return Observable.of(physician)
            })
            .subscribe((res: Physician)=>{

                this.physician = res;
                this.groups = res.groups;

                this.ready = true;
            })
    }

    ngOnDestroy(){
        this.subs.unsubscribe();
    }


    addGroup(){
        this.groups.push( _.assign(new PhysicianGroup(), {name: "Категория "+(this.groups.length+1), percents: 1, persons: 1}))
    }

    removeGroup(i: number){
        this.groups.splice(i,1);
    }


    isDisabled(){
        let allPercents: number = 0;
        let allPersons: number = 0;
        let hasEmptyGroup: boolean = false;

        this.groups.map((group: PhysicianGroup)=>{
            allPercents += group.percents;
            allPersons +=group.persons;

            if(!hasEmptyGroup && group.percents <= 0 || group.name == ""){
                hasEmptyGroup = true;
            }
        });

        return allPercents != 100 || hasEmptyGroup || (this.physician.name == "") || (!this.physician.persons || this.physician.persons < 0) || this.physician.persons  != allPersons
    }

    onCreatePhysician(){
        this.physician.groups = this.groups;

        this.data.update(this.physician);
        this.router.navigate(["../"], {relativeTo: this.state});
    }
}