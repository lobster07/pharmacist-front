import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import * as _ from "lodash";
import {PlanDetailed} from "../../../../../shared/entities/plan-detailed";
import {PlansDataService} from "../../../../../shared/services/data/pharmacist/plans.service";
import {DrugsDataService} from "../../../../../shared/services/data/pharmacist/drugs.service";
import {PhysiciansDataService} from "../../../../../shared/services/data/pharmacist/physicians.service";
import {Observable} from "rxjs/Observable";
import {PhysicianItem, Plan} from "../../../../../shared/entities/plan";
import {Drug} from "../../../../../shared/entities/drug";
import {Physician} from "../../../../../shared/entities/physician";

@Component({
    selector: 'plans-list',
    templateUrl: './plans-list.component.html'
})

export class PlansListComponent implements OnInit, OnDestroy{

    private detailedPlans: PlanDetailed[] = [];

    private subs: Subscription;

    constructor(
        private plans: PlansDataService,
        private drugs: DrugsDataService,
        private physicians: PhysiciansDataService,
    ){}

    ngOnInit(){

        this.subs = Observable.combineLatest(this.plans.store, this.drugs.store, this.physicians.store)
            .map((res: any)=>{

                let plans: Plan[] = res[0];
                let drugs: Drug[] = res[1];
                let physicians: Physician[] = res[2];

                let arr: PlanDetailed[] = [];

                plans.map((plan: Plan)=>{

                    arr.push(_.assign(new PlanDetailed(),{
                        id: plan.id,
                        name: plan.name,
                        count: plan.count,
                        drug: _.find(drugs, {id: plan.drugId}),
                        physiciansList: _.map(plan.physiciansList, (item: PhysicianItem)=>{
                            return {
                                physician: _.find(physicians, {id: item.physicianId}),
                                percents: item.percents
                            }
                        })
                    }))
                });

                return arr
            })
            .subscribe((res: any)=>{
                this.detailedPlans = res;
            });

    }

    ngOnDestroy(){
        this.subs.unsubscribe();
    }

    onRemove(p: PlanDetailed){

        console.log(p.id, p);
        this.plans.remove(p.id);
    }
}