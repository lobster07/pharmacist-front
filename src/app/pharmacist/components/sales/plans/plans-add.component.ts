import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as _ from "lodash";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {PhysicianItem, Plan} from "../../../../../shared/entities/plan";
import {Physician} from "../../../../../shared/entities/physician";
import {Drug} from "../../../../../shared/entities/drug";
import {PlansDataService} from "../../../../../shared/services/data/pharmacist/plans.service";
import {DrugsDataService} from "../../../../../shared/services/data/pharmacist/drugs.service";
import {PhysiciansDataService} from "../../../../../shared/services/data/pharmacist/physicians.service";
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'plan-add',
    templateUrl: './plans-add.component.html'
})

export class PlansAddComponent implements OnInit, OnDestroy{

    private subs: Subscription;

    private data: {
        plan: Plan,
        physicians: Physician[],
        drugs: Drug[]
    };

    private selectedPhysicians: string[] =[];
    private list: PhysicianItem[]= [];

    private emptyDrugs: boolean;
    private emptyPhysicians: boolean;

    private ready: boolean;

    constructor(
        private state: ActivatedRoute,
        private router: Router,
        private plans: PlansDataService,
        private drugs: DrugsDataService,
        private physicians: PhysiciansDataService
    ){}

    ngOnInit(){

        this.subs = Observable.zip(
            this.state.params,
            this.plans.store,
            this.drugs.store,
            this.physicians.store
        ).flatMap((res: any)=>{

            let params: Params = res[0];
            let drugs: Drug[] = res[2];
            let physicians: Physician[] = res[3];
            let plans: Plan[] = res[1];

            let plan: Plan;

            if(params['id']){
                plan = _.clone(_.find(plans, {id: params['id']}));
            }else{
                plan = _.assign(new Plan(),{name: "Новый план"});
                plan.drugId = drugs[0].id
            }

            return Observable.of({plan, physicians, drugs})
        })
        .subscribe((res: any)=>{
            this.data = res;

            this.emptyDrugs = _.isEmpty(res.drugs);
            this.emptyPhysicians = _.isEmpty(res.physicians);
            this.list = res.plan.physiciansList;
            this.selectedPhysicians = _.map(res.plan.physiciansList,(item: PhysicianItem)=>{
                return item.physicianId;
            });

            this.ready = true;
        })
    }

    ngOnDestroy(){
        this.subs.unsubscribe();
    }

    isDataEmpty(){
        return this.emptyPhysicians || this.emptyDrugs
    }

    isDisabled(){

        let allPercents: number = 0;
        let hasEmptyGroup: boolean = false;

        this.list.map((item: PhysicianItem)=>{
            allPercents += item.percents;

            if(!hasEmptyGroup && item.percents <= 0){
                hasEmptyGroup = true;
            }
        });

        return allPercents != 100 || hasEmptyGroup || (this.data.plan.name == "") || (!this.data.plan.count || this.data.plan.count < 0)
    }

    onChangeSelectedPhysicians(){
        console.log(this.selectedPhysicians);

        var filteredList = _.filter(this.list, (item: PhysicianItem)=>{
            return _.includes(this.selectedPhysicians, item.physicianId)
        });

        _.map(this.selectedPhysicians,(item: string)=>{
            if(!_.find(filteredList,{physicianId: item})){
                filteredList.push({
                    physicianId: item,
                    percents: 30
                })
            }
        });

        this.list = filteredList;

    }

    getPhysicianName(id: string):string{
        return _.find(this.data.physicians, {id:id}).name
    }

    onCreatePlan(){

        this.data.plan.physiciansList = this.list;

        this.plans.update(this.data.plan);

        this.router.navigate(["../"], {relativeTo: this.state});
    }
}