import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as _ from "lodash";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";
import {Drug} from "../../../../../shared/entities/drug";
import {DrugsDataService} from "../../../../../shared/services/data/pharmacist/drugs.service";

@Component({
    selector: 'drugs-add',
    templateUrl: './drugs-add.component.html'
})

export class DrugsAddComponent implements OnInit, OnDestroy{

    private subs: Subscription;
    private drug: Drug;

    private ready: boolean;

    constructor(
        private state: ActivatedRoute,
        private router: Router,
        private data: DrugsDataService
    ){}

    ngOnInit(){

        this.subs = Observable.zip(
            this.state.params,
            this.data.store
        ).flatMap((res: any)=>{
            let params: Params = res[0];
            let drugs: Drug[] = res[1];
            let drug: Drug;

            if(params['id']){
                drug = _.clone(_.find(drugs, {id: params['id']}));
            }else{
                drug = _.assign(new Drug(),{
                    name: "Новый медикамент"
                })
            }

            return Observable.of(drug)
        })
        .subscribe((res: Drug)=>{

            this.drug = res;

            this.ready = true;
        })
    }

    ngOnDestroy(){
        this.subs.unsubscribe();
    }


    isDisabled(){
       return this.drug.name == "";
    }

    onCreateDrug(){
        this.data.update(this.drug);
        this.router.navigate(["../"], {relativeTo: this.state});
    }
}