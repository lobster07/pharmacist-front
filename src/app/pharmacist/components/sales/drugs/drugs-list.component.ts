import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {Drug} from "../../../../../shared/entities/drug";
import {DrugsDataService} from "../../../../../shared/services/data/pharmacist/drugs.service";

@Component({
    selector: 'drugs-list',
    templateUrl: './drugs-list.component.html'
})

export class DrugsListComponent implements OnInit, OnDestroy{

    private drugs: Drug[];

    private subs: Subscription;

    constructor(
        private data: DrugsDataService
    ){}

    ngOnInit(){
        this.subs = this.data.store.subscribe((arr: Drug[])=>{
            this.drugs = arr;
        });
    }

    ngOnDestroy(){
        this.subs.unsubscribe();
    }

    onRemove(p: Drug){
        this.data.remove(p.id);
    }
}