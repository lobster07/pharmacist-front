import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {SessionService} from "../../../shared/services/session.service";

@Component({
    selector: 'pharmacist',
    templateUrl: './pharmacist.component.html'
})

export class PharmacistComponent implements OnInit, OnDestroy{

    private me: any;

    constructor(
        private session: SessionService,
        private router: Router
    ){}

    ngOnInit(){
       this.me = this.session.getItem('me');
    }

    ngOnDestroy(){

    }

    onLogout(){
       this.router.navigate(["/login"], {replaceUrl: true});
       this.session.clearSession();
    }
}