import {NgModule} from "@angular/core";
import {BrowserModule}  from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {RouterModule} from '@angular/router';
import {FormsModule}  from '@angular/forms';
import { SharedModule } from "../../shared/shared.module";

import { SalesComponent } from "./components/sales/sales.component";
import { PharmacistComponent } from "./components/pharmacist.component";
import { PhysiciansLayoutComponent } from "./components/sales/physicians/physicians-layout.component";
import { PhysiciansListComponent } from "./components/sales/physicians/physicians-list.component";
import { PhysicianAddComponent } from "./components/sales/physicians/physician-add.component";

import { DrugsLayoutComponent } from "./components/sales/drugs/drugs-layout.component";
import { DrugsListComponent } from "./components/sales/drugs/drugs-list.component";
import { DrugsAddComponent } from "./components/sales/drugs/drugs-add.component";

import { PlansLayoutComponent } from "./components/sales/plans/plans-layout.component";
import { PlansListComponent } from "./components/sales/plans/plans-list.component";
import { PlansAddComponent } from "./components/sales/plans/plans-add.component";

import { DashboardComponent } from "./components/sales/dashboard/dashboard.component"

import { VisitsComponent } from "./components/visits/visits.component";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule,
        FormsModule,
        SharedModule,
    ],
    declarations: [
        SalesComponent,
        PharmacistComponent,
        PhysiciansLayoutComponent,
        PhysiciansListComponent,
        PhysicianAddComponent,
        DrugsLayoutComponent,
        DrugsListComponent,
        DrugsAddComponent,
        PlansLayoutComponent,
        PlansListComponent,
        PlansAddComponent,
        DashboardComponent,
        VisitsComponent
    ]
})
export class PharmacistModule { }
